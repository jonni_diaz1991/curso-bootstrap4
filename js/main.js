$(function () {
    $('[data-toggle="tooltip"]').tooltip()
    $('[data-toggle="popover"]').popover()
    $('.carousel').carousel({
        interval: 2000
    })
    $('.btn-comprar').on('click', function() {
        $(this).removeClass('btn-primary');
        $(this).addClass('btn-success');         
      });
    $('#contacto').on('show.bs.modal', function (e) {
        console.log('Se esta mostrando el modal.')
    })
    $('#contacto').on('shown.bs.modal', function (e) {
        console.log('Se mostró el modal.')
    })
    $('#contacto').on('hide.bs.modal', function (e) {
        console.log('Se esta ocultando el modal.')
    })
    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log('Se ocultó el modal.')
        $('.btn-comprar').addClass('btn-primary');
        $('.btn-comprar').removeClass('btn-success');     
    })
    $('#contacto').on('hidePrevented.bs.modal', function (e) {
        console.log('Se esta evitando que se oculte el modal.')
    })
})